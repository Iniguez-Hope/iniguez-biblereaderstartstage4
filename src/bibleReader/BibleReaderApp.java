package bibleReader;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.BookOfBible;
import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.ResultType;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * The main class for the Bible Reader Application.
 * 
 * @author Angel Iniguez & Ryan Yonker
 */
@SuppressWarnings("serial")
public class BibleReaderApp extends JFrame {
	public static final int width = 800;
	public static final int height = 600;

	public static void main(String[] args) {
		new BibleReaderApp();
	}

	private BibleReaderModel model;
	private ResultView resultView;
	JMenuBar menuBar;
	private JTextField textField;
	private JButton search;
	private JPanel panel;
	private Container contents;
	private JButton passage;
	private JFileChooser fc;
	private NavigableResults navWindow;
	/**
	 * Default constructor. We may want to replace this with a different one.
	 */
	public BibleReaderApp() {
	
		model = new BibleReaderModel();
										
		File kjvFile = new File("kjv.atv");
		VerseList verses = BibleIO.readBible(kjvFile);
		Verse v = new Verse(BookOfBible.Dummy, 1, 1, "");
		verses.add(v);
		
		File esvFile = new File("esv.atv");
		VerseList verse = BibleIO.readBible(esvFile);
		Verse ver = new Verse(BookOfBible.Dummy, 1, 1, "");
		verse.add(ver);
		
		File asvFile = new File("asv.xmv");
		VerseList versesList = BibleIO.readBible(asvFile);
		Verse verList = new Verse(BookOfBible.Dummy, 1, 1, "");
		verses.add(verList);

		Bible kjv = new ArrayListBible(verses);
		Bible esv = new ArrayListBible(verse);
		Bible asv = new ArrayListBible(versesList);
		
		model.addBible(esv);
		model.addBible(asv);
		model.addBible(kjv);
		
		resultView = new ResultView(model);

		setupGUI();
		pack();
		setSize(width, height);

		// So the application exits when you click the "x".
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Set up the main GUI. Make sure you don't forget to put resultView
	 * somewhere!
	 */
	private void setupGUI() {

		setTitle("Bible Reader");
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		textField = new JTextField(30);
		textField.setName("InputTextField");
		textField.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(textField.getText().contains(":")){
					getPassage();
				}
				else{
					doSearch();
				}
			}
		});
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		passage = new JButton("Passage Lookup");
		passage.setName("PassageButton");
		passage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getPassage();
			}
		});
		
		// Add a button to search.
		search = new JButton("Search");
		search.setName("SearchButton");
		search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doSearch();
			}
		});
		JMenu file = new JMenu("File");
		menuBar.add(file);
		
		JMenuItem open = new JMenuItem("Open");
        open.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                readData();
            }
        });
        file.add(open);
		//Adds the action performed when exit option is pressed.
        JMenuItem exit = new JMenuItem("Exit");
        exit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        file.add(exit);
        
        
        
      //Adds the help option to about menu.
        JMenu help = new JMenu("Help");
        menuBar.add(help);
        JMenuItem about = new JMenuItem("About");
        help.add(about);
        about.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	JOptionPane.showMessageDialog(contents, "Angel Iniguez & Ryan Yonker");
            }
        });
   		
		panel = new JPanel();
		panel.setBackground(Color.getHSBColor(7, 52, 100));
		panel.add(textField);
		panel.add(search);
		panel.add(passage);
		add(panel);

		setJMenuBar(menuBar);
		contents = getContentPane();
		contents.add(resultView, BorderLayout.CENTER);
		contents.add(panel, BorderLayout.NORTH);

	}

	// The stage numbers below may change, so make sure to pay attention to
	// what the assignment says.
	// TODO Add passage lookup: Stage ?
	// TODO Add 2nd version on display: Stage ?
	// TODO Limit the displayed search results to 20 at a time: Stage ?
	// TODO Add 3rd versions on display: Stage ?
	// TODO Format results better: Stage ?
	// TODO Display cross references for third version: Stage ?
	// TODO Save/load search results: Stage ?

	private void readData() {
		if (fc == null) {
			fc = new JFileChooser();
		}
		fc.setCurrentDirectory(new File("/c:"));
		int returnVal = fc.showOpenDialog(new JFrame());
				

		// We check whether or not they clicked the "Open" button
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			// We get a reference to the file that the user selected.
			File file = fc.getSelectedFile();
			// Make sure it actually exists.
			if (!file.exists()) {
				showFileNotFoundDialog();
			} else {
				try {
					
					VerseList verses1 = BibleIO.readBible(file);

					Bible title = new ArrayListBible(verses1);
					
					model.addBible(title);
					
				} catch (Exception e) {
					showErrorDialog();
				}
			}
		}
	}
	
	private void showFileNotFoundDialog() {
		JOptionPane.showMessageDialog(this, "That file does not exist!",
				"File not found", JOptionPane.ERROR_MESSAGE);
	}
	
	private void showErrorDialog() {
		JOptionPane.showMessageDialog(this,
				"There was a problem reading/writing the file.", "Error",
				JOptionPane.ERROR_MESSAGE);
	}
	
	public void doSearch() {
		String text = textField.getText();
		if(!text.isEmpty()){
		ArrayList<Reference> refs = new ArrayList<Reference>();
		refs = model.getReferencesContaining(text); 
		navWindow = new NavigableResults(refs,textField.getText(),ResultType.SEARCH);
		}
		
		resultView.setResults(navWindow);
		
	}
	
	public void getPassage() {
		ArrayList<Reference> refs = model.getReferencesForPassage(textField.getText());
		navWindow = new NavigableResults(refs,textField.getText(),ResultType.PASSAGE);
		resultView.setResults(navWindow);
	}
}


