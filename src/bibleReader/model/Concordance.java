package bibleReader.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeSet;

/**
 * Concordance is a class which implements a concordance for a Bible. In other words, it allows the easy lookup of all
 * references which contain a given word.
 * 
 * @author Chuck Cusack, March 2013 (Provided the interface)
 * @author ?, March 2013 (Provided the implementation details)
 */
public class Concordance {
	// Add fields here.  (I actually only needed one field.)
	private HashMap<String, ArrayList<Reference>> map;

	/**
	 * Construct a concordance for the given Bible.
	 */
	public Concordance(Bible bible) {
		map = new HashMap<String, ArrayList<Reference>>();
		VerseList verselist = bible.getAllVerses();
		for (Verse verse : verselist) {
			String text = verse.getText();
			for (String word : extractWords(text)) {
				
				if (!(map.containsKey(word))) {
					map.put(word, new ArrayList<Reference>());
					map.get(word).add(verse.getReference());
				}
				if (map.containsKey(word)) {
					ArrayList<Reference> referencelist = map.get(word);
					if (!((referencelist.get(referencelist.size() - 1)
							.equals(verse.getReference())))) {
						referencelist.add(verse.getReference());
					}
				}
			}
		}
	}
	
	public static ArrayList<String> extractWords(String text) {
		text = text.toLowerCase();
		text = text.replaceAll("(<sup>[,\\w]*?</sup>|'s|'s|&#\\w*;)", " ");
		text.replaceAll(",", "");
		String[] words = text.split("\\W+");
		ArrayList<String> toRet = new ArrayList<String>(Arrays.asList(words));
		toRet.remove("");
		return toRet;
	}

	/**
	 * Return the list of references to verses that contain the word 'word' (ignoring case) in the version of the Bible
	 * that this concordance was created with.
	 * 
	 * @param word a single word (no spaces, etc.)
	 * @return the list of References of verses from this version that contain the word, or an empty list if no verses
	 *         contain the word.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Reference> getReferencesContaining(String word) {
		word = word.toLowerCase();
		ArrayList<Reference> references = new ArrayList<Reference>();
		if (!(map.get(word) == null)) {
			references = (ArrayList<Reference>) map.get(word).clone();
		}
		return references;
	}

	/**
	 * Given an array of Strings, where each element of the array is expected to be a single word (with no spaces, etc.,
	 * but ignoring case), return a ArrayList<Reference> containing all of the verses that contain <i>all of the words</i>.
	 * 
	 * @param words A list of words.
	 * @return An ArrayList<Reference> containing references to all of the verses that contain all of the given words, or an
	 *         empty list if
	 */
	public ArrayList<Reference> getReferencesContainingAll(ArrayList<String> words) {
		ArrayList<Reference> references = (getReferencesContaining(words.get(0)));
		ArrayList<String> list = new ArrayList<String>();
		
		list.add(words.get(0).toLowerCase());
		for (int i = 1; i < words.size(); i++) {
			String text = words.get(i).toLowerCase().replace(",", "");
			if (!(list.contains(text))) {
				list.add(text);
				references.retainAll(new TreeSet<Reference>(getReferencesContaining(text)));
			}
		}

		return references;
	}
}
