package bibleReader.model;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author Angel Iniguez Jr
 */
public class TreeMapBible implements Bible {

	// The Fields
	private String version;
	private TreeMap<Reference, String> theVerses;
	private String title;

	// Or replace the above with:
	// private TreeMap<Reference, Verse> theVerses;
	// Add more fields as necessary.

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param version the version of the Bible (e.g. ESV, KJV, ASV, NIV).
	 * @param verses  All of the verses of this version of the Bible.
	 */
	public TreeMapBible(VerseList verses) {
		theVerses = new TreeMap<Reference, String>();
		for (Verse verse : verses) {
			theVerses.put(verse.getReference(), verse.getText());
		}
		version = verses.getVersion();
		title = verses.getDescription();
	}

	@Override
	public int getNumberOfVerses() {
		return theVerses.size();
	}

	@Override
	public VerseList getAllVerses() {
		VerseList verses = new VerseList(version, title);
		Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> entry : mySet) {
			verses.add(new Verse(entry.getKey(), entry.getValue()));
		}
		return verses;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> entry : mySet) {
			if (entry.getKey().equals(ref)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getVerseText(Reference r) {
		Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> entry : mySet) {
			if (entry.getKey().equals(r)) {
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(Reference r) {
		//Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> entry : theVerses.entrySet()) {
			if (entry.getKey().equals(r)) {
				return new Verse(entry.getKey(), entry.getValue());
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		return getVerse(ref);
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	//
	// For Stage 11 the first two methods below will be implemented as specified in
	// the comments.
	// Do not over think these methods. All three should be pretty straightforward
	// to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they work
	// better.
	// At that stage you will create another class to facilitate searching and use
	// it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList verses = new VerseList(version, title);
		phrase = phrase.toLowerCase();
		if (phrase == "") {
			return verses;
		}
		Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> entry : mySet) {
			if (entry.getValue().toLowerCase().contains(phrase)) {
				verses.add(new Verse(entry.getKey(), entry.getValue()));
			}
		}
		return verses;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		phrase = phrase.toLowerCase();
		if (phrase == "") {
			return references;
		}
		Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> entry : mySet) {
			if (entry.getValue().toLowerCase().contains(phrase)) {
				references.add(entry.getKey());
			}
		}
		return references;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList verses = new VerseList(version, "Arbitrary list of Verses");
		for (Reference reference : references) {
			verses.add(getVerse(reference));
//			if (getVerse(reference) == null) {
//				verses.add(null);
//			} else {
//				verses.add(getVerse(reference));
//			}
		}
		return verses;
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 11.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented by
	// looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		if (getBook(book) != null) {
			VerseList chapterVerses = getChapter(book, chapter);
			return chapterVerses.get(chapterVerses.size() - 1).getReference().getVerse();
		}
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		if (getBook(book) != null) {
			VerseList bookVerses = getBook(book);
			return bookVerses.get(bookVerses.size() - 1).getReference().getChapter();
		}
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		if (firstVerse.compareTo(lastVerse) < 0) {
			SortedMap<Reference, String> s = theVerses.subMap(firstVerse, true, lastVerse, true);
			Set<Map.Entry<Reference, String>> mySet = s.entrySet();
			for (Map.Entry<Reference, String> element : mySet) {
				references.add(element.getKey());
			}
			return references;
		}
		return references;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		if (firstVerse.compareTo(lastVerse) < 0) {
			SortedMap<Reference, String> s = theVerses.subMap(firstVerse, true, lastVerse, false);
			Set<Map.Entry<Reference, String>> mySet = s.entrySet();
			for (Map.Entry<Reference, String> element : mySet) {
				references.add(element.getKey());
			}
			return references;
		}
		return references;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		if (book != null) {
			Reference ref1 = new Reference(book, 1, 1);
			Reference ref2 = new Reference(BookOfBible.nextBook(book), 1, 1);
			return getReferencesExclusive(ref1, ref2);
		}
		return new ArrayList<Reference>();
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		for (Verse verse : getChapter(book, chapter)) {
			references.add(verse.getReference());
		}
		return references;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		for (Verse verse : getChapters(book, chapter1, chapter2)) {
			references.add(verse.getReference());
		}
		return references;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		for (Verse verse : getPassage(book, chapter, verse1, verse2)) {
			references.add(verse.getReference());
		}
		return references;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		for (Verse verse : getPassage(book, chapter1, verse1, chapter2, verse2)) {
			references.add(verse.getReference());
		}
		return references;
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);
		if (firstVerse.compareTo(lastVerse) > 0 && isValid(firstVerse)) {
			return someVerses;
		}
		SortedMap<Reference, String> s = theVerses.subMap(firstVerse, true, lastVerse, true);
		Set<Map.Entry<Reference, String>> mySet = s.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			someVerses.add(aVerse);
		}
		return someVerses;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// Implementation of this method provided by Chuck Cusack.
		// This is provided so you have an example to help you get started
		// with the other methods.

		// We will store the resulting verses here. We copy the version from
		// this Bible and set the description to be the passage that was searched for.
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);

		// Make sure the references are in the correct order. If not, return an empty
		// list.
		if (firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, String> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, String>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			someVerses.add(aVerse);
		}
		return someVerses;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		VerseList verses = new VerseList("", "");
		if (book != null) {
			Reference ref1 = new Reference(book, 1, 1);
			Reference ref2 = new Reference(BookOfBible.nextBook(book), 1, 1);
			verses = new VerseList(this.getVersion(), book.toString());

			VerseList list = getVersesExclusive(ref1, ref2);
			for (Verse verse : list) {
				verses.add(verse);
			}
			return verses;
		}
		return verses;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		Reference ref1 = new Reference(book, chapter, 1);
		Reference ref2 = new Reference(book, chapter + 1, 1);
		VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter);
		VerseList list = getVersesExclusive(ref1, ref2);
		for (Verse verse : list) {
			verses.add(verse);
		}
		return verses;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference ref1 = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2 + 1, 1);
		VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter1 + " - " + chapter2);
		VerseList list = getVersesExclusive(ref1, ref2);
		for (Verse verse : list) {
			verses.add(verse);
		}
		return verses;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);
		VerseList verses = new VerseList(this.getVersion(),
				book.toString() + " " + chapter + " : " + verse1 + " - " + verse2);

		VerseList list = getVersesInclusive(ref1, ref2);
		for (Verse verse : list) {
			verses.add(verse);
		}
		return verses;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2);
		VerseList verses = new VerseList(this.getVersion(),
				book.toString() + " " + chapter1 + " : " + verse1 + " - " + chapter2 + " : " + verse2);
		VerseList list = getVersesInclusive(ref1, ref2);
		for (Verse verse : list) {
			verses.add(verse);
		}
		return verses;
	}

}
