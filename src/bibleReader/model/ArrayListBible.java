package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Angel Iniguez Jr (provided the implementation)
 */
public class ArrayListBible implements Bible {

	private String version;
	private String title;
	private ArrayList<Verse> verses;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		this.verses = verses.copyVerses();
		version = verses.getVersion();
		title = verses.getDescription();
	}

	@Override
	public int getNumberOfVerses() {
		return verses.size();
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		for (Verse verse : verses) {
			if (verse.getReference().equals(ref)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getVerseText(Reference r) {
		for (Verse verse : verses) {
			if (verse.getReference().equals(r)) {
				return verse.getText();
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(Reference r) {
		for (Verse verse : verses) {
			if (verse.getReference().equals(r)) {
				return verse;
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		for (Verse theVerse : verses) {
			if (theVerse.getReference().equals(ref)) {
				return theVerse;
			}
		}
		return null;
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	// See the Bible interface for the documentation of these methods.
	// Do not over think these methods. All three should be pretty
	// straightforward to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they
	// work better.
	// At that stage you will create another class to facilitate searching and
	// use it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	@Override
	public VerseList getAllVerses() {
		VerseList list = new VerseList(version, title, verses);
		return list;
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList list = new VerseList(version, title);
		if (phrase == "") {
			return list;
		}
		for (Verse verse : verses) {
			if (verse.getText().toLowerCase().contains(phrase.toLowerCase())) {
				list.add(verse);
			}
		}
		return list;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> list = new ArrayList<Reference>();
		if (phrase == "") {
			return list;
		}
		for (Verse verse : verses) {
			if (verse.getText().toLowerCase().contains(phrase.toLowerCase())) {
				list.add(verse.getReference());
			}
		}
		return list;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList verses = new VerseList(version, "Arbitrary list of Verses");
		for (Reference reference : references) {
			Verse verse = getVerse(reference);
			if (verse == null) {
				verses.add(null);
			} else {
				verses.add(verse);
			}
		}
		return verses;
	}
	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 7.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		if (getBook(book) != null) {
			VerseList chapterVerses = getChapter(book, chapter);
			return chapterVerses.get(chapterVerses.size() - 1).getReference().getVerse();
		}
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		if (getBook(book) != null) {
			VerseList bookVerses = getBook(book);
			return bookVerses.get(bookVerses.size() - 1).getReference().getChapter();
		}
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		int index = 0;
		ArrayList<Reference> references = new ArrayList<Reference>();
		Reference ref;
		if (firstVerse.compareTo(lastVerse) <= 0) {
			for (Verse verse : verses) {
				if (verse.getReference().equals(firstVerse)) { // Works as a pseudo-isValid
					index = verses.indexOf(verse);
					references.add(firstVerse);
					break;
				}
			}

			if (firstVerse.compareTo(lastVerse) == 0) {
				return references;
			}

			if (!(references.isEmpty())) {
				int num = 0;
				for (int i = index + 1; i < verses.size(); i++) {
					ref = verses.get(i).getReference();
					if (ref.compareTo(lastVerse) == 0) {
						references.add(ref);
						return references;
					} else if (ref.compareTo(lastVerse) < 0) {
						num = i;
						references.add(ref);
					} else if (!(ref.equals(lastVerse))) {
						references.clear(); // The last verse looked at is not the lastVerse passed in
					}
				}
			}
		}
		return references;
	}

	@Override

	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		int index = 0;
		ArrayList<Reference> references = new ArrayList<Reference>();
		Reference ref;
		if (firstVerse.compareTo(lastVerse) < 0 && isValid(firstVerse)) {
			for (Verse verse : verses) {
				if (verse.getReference().equals(firstVerse)) {
					index = verses.indexOf(verse);
					references.add(firstVerse);
					break;
				}
			}
			for (int i = index + 1; i < verses.size(); i++) {
				ref = verses.get(i).getReference();
				if (ref.compareTo(lastVerse) < 0) {
					references.add(ref);
				}
			}
		}
		return references;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		Reference ref1 = new Reference(book, 1, 1);
		Reference ref2 = new Reference(BookOfBible.nextBook(book), 1, 1);
		return getReferencesExclusive(ref1, ref2);
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		for (Verse verse : getChapter(book, chapter)) {
			references.add(verse.getReference());
		}
		return references;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		for (Verse verse : getChapters(book, chapter1, chapter2)) {
			references.add(verse.getReference());
		}
		return references;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		for (Verse verse : getPassage(book, chapter, verse1, verse2)) {
			references.add(verse.getReference());
		}
		return references;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		ArrayList<Reference> references = new ArrayList<Reference>();
		for (Verse verse : getPassage(book, chapter1, verse1, chapter2, verse2)) {
			references.add(verse.getReference());
		}
		return references;
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		int index = 0;
		VerseList verseList = new VerseList(getVersion(), getTitle());
		if (firstVerse.compareTo(lastVerse) <= 0) {
			for (Verse verse : verses) {
				if (verse.getReference().equals(firstVerse)) { // Works as a pseudo-isValid
					index = verses.indexOf(verse);
					verseList.add(verse);
					break;
				}
			}

			if (firstVerse.compareTo(lastVerse) == 0) {
				return verseList;
			}

			if (!(verseList.size() == 0)) {
				//int num = 0;
				for (int i = index + 1; i < verses.size(); i++) {
					Verse ref = verses.get(i);
					Reference refRef = ref.getReference();
					if (refRef.compareTo(lastVerse) == 0) {
						verseList.add(ref);
						return verseList;
					} else if (refRef.compareTo(lastVerse) < 0) {
						//num = i;
						verseList.add(ref);
					} else if (!(refRef.equals(lastVerse))) {
						verseList.clear(); // The last verse looked at is not the lastVerse passed in
					}
				}
			}
		}
		return verseList;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		int index = 0;
		VerseList verseList = new VerseList(getVersion(), getTitle());
		if (firstVerse.compareTo(lastVerse) < 0 && isValid(firstVerse)) {
			for (Verse verse : verses) {
				if (verse.getReference().equals(firstVerse)) {
					index = verses.indexOf(verse);
					verseList.add(verse);
					break;
				}
			}
			for (int i = index + 1; i < verses.size(); i++) {
				Verse verse = verses.get(i);
				Reference ref = verse.getReference();
				if (ref.compareTo(lastVerse) < 0) {
					verseList.add(verse);
				}
			}
		}
		return verseList;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		VerseList verseList = new VerseList("", "");
		if (book != null) {
			Reference ref1 = new Reference(book, 1, 1);
			Reference ref2 = new Reference(BookOfBible.nextBook(book), 1, 1);
			verseList = new VerseList(this.getVersion(), book.toString());
			VerseList list = getVersesExclusive(ref1, ref2);
			for (Verse verse : list) {
				verseList.add(verse);
			}
		}
		return verseList;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		Reference ref1 = new Reference(book, chapter, 1);
		Reference ref2 = new Reference(book, chapter + 1, 1);
		VerseList verseList = new VerseList(this.getVersion(), book.toString() + " " + chapter);
		VerseList list = getVersesExclusive(ref1, ref2);
		for (Verse verse : list) {
			verseList.add(verse);
		}
		return verseList;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference ref1 = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2 + 1, 1);
		VerseList verseList = new VerseList(this.getVersion(), book.toString() + " " + chapter1 + " - " + chapter2);
		VerseList list = getVersesExclusive(ref1, ref2);
		for (Verse verse : list) {
			verseList.add(verse);
		}
		return verseList;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2 + 1);
		VerseList verseList = new VerseList(this.getVersion(),
				book.toString() + " " + chapter + " : " + verse1 + " - " + verse2);
		VerseList list = getVersesExclusive(ref1, ref2);
		for (Verse verse : list) {
			verseList.add(verse);
		}
		return verseList;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2 + 1);
		VerseList verseList = new VerseList(this.getVersion(),
				book.toString() + " " + chapter1 + " : " + verse1 + " - " + chapter2 + " : " + verse2);
		VerseList list = getVersesExclusive(ref1, ref2);
		for (Verse verse : list) {
			verseList.add(verse);
		}
		return verseList;
	}
}
