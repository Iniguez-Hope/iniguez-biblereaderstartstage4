package bibleReader.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The model of the Bible Reader. It stores the Bibles and has methods for
 * searching for verses based on words or references.
 * 
 * @author cusack
 */
public class BibleReaderModel implements MultiBibleModel {

	ArrayList<Bible> bibleList;
	String[] versionList;
	private HashMap<Bible, Concordance> bibleVersions;
	
	public static final String number = "\\s*(\\d+)\\s*";
	public static Pattern bookPattern = Pattern
			.compile("\\s*((?:1|2|3|I|II|III)\\s*\\w+|(?:\\s*[a-zA-Z]+)+)\\s*(.*)");
	public static Pattern cPattern = Pattern.compile(number);
	public static Pattern cvPattern = Pattern.compile(number + ":" + number);
	public static Pattern ccPattern = Pattern.compile(number + "-" + number);
	public static Pattern cvvPattern = Pattern.compile(number + ":" + number
			+ "-" + number);
	public static Pattern ccvPattern = Pattern.compile(number + "-" + number
			+ ":" + number);
	public static Pattern cvcvPattern = Pattern.compile(number + ":" + number
			+ "-" + number + ":" + number);

	/**
	 * Default constructor. You probably need to instantiate objects and do other
	 * assorted things to set up the model.
	 */
	public BibleReaderModel() {
		bibleList = new ArrayList<Bible>();
		bibleVersions = new HashMap<Bible, Concordance>();
	}

	/**
	 * Returns an Array with the string name of the versions
	 */
	@Override
	public String[] getVersions() {
		versionList = new String[bibleList.size()];
		int i = 0;
		while (i < bibleList.size()) {
			for (Bible b : bibleList) {
				versionList[i] = b.getVersion();
				i++;
			}
		}
		Arrays.sort(versionList);
		return versionList;
	}

	/**
	 * Returns the number of versions. 
	 */
	@Override
	public int getNumberOfVersions() {
		if (bibleList.isEmpty()) {
			return 0;
		} else {
			return bibleList.size();
		}
	}

	/**
	 * Adds a bible to the ArrayList of Bibles
	 */
	@Override
	public void addBible(Bible bible) {
		bibleList.add(bible);
	}

	/**
	 * Lets you get a Bible based on the version
	 */
	@Override
	public Bible getBible(String version) {
		for (Bible b : bibleList) {
			if (b.getVersion().equals(version)) {
				return b;
			}
		}
		return null;
	}

	/**
	 * Returns an ArrayList of all the References that are containing a certain word. 
	 */
	@Override
	public ArrayList<Reference> getReferencesContaining(String words) {
		TreeSet<Reference> tree = new TreeSet<Reference>();
		for (Bible bible : bibleList) {
			tree.addAll(bible.getReferencesContaining(words));
		}
		ArrayList<Reference> ref = new ArrayList<Reference>(tree);
		return ref;
	}

	/**
	 * Returns an VerserList of all the Verses that match a Reference in the ArrayList
	 * and also matches a version in the bible.
	 */
	@Override
	public VerseList getVerses(String version, ArrayList<Reference> references) {
		return getBible(version).getVerses(references);
	}
	// ---------------------------------------------------------------------

	@Override
	public String getText(String version, Reference reference) {
		Bible bible = getBible(version);
		if(bible == null){
			return "";
		}
		if (bible.isValid(reference)) {
			return bible.getVerse(reference).getText();
		}
		return "";
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(String reference) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		String theRest = null;
		String book = null;
		int chapter1, chapter2, verse1, verse2;

		// First, split the input into the book and the rest, if possible.
		Matcher m = bookPattern.matcher(reference);
		try {
			// Now see if it matches.
			if (m.matches()) {
				// It matches. Good.
				book = m.group(1);
				theRest = m.group(2);
				// Now we need to parse theRest to see what format it is.
				// Notice that I have omitted some of the cases.
				// You should think about whether or not the order the
				// possibilities occurs matters if you use this and add more
				// cases.
				if (theRest.length() == 0) {
					refs = getBookReferences(BookOfBible.getBookOfBible(book));
				} else if ((m = cPattern.matcher(theRest)).matches()) {
					chapter1 = Integer.parseInt(m.group(1));
					// chapter1:verse1-chapter2:verse2
					refs = getChapterReferences(
							BookOfBible.getBookOfBible(book), chapter1);
				} else if ((m = cvcvPattern.matcher(theRest)).matches()) {
					chapter1 = Integer.parseInt(m.group(1));
					verse1 = Integer.parseInt(m.group(2));
					chapter2 = Integer.parseInt(m.group(3));
					verse2 = Integer.parseInt(m.group(4));
					// chapter1:verse1-chapter2:verse2
					refs = getPassageReferences(
							BookOfBible.getBookOfBible(book), chapter1, verse1,
							chapter2, verse2);
				} else if ((m = cvPattern.matcher(theRest)).matches()) {
					chapter1 = Integer.parseInt(m.group(1));
					verse1 = Integer.parseInt(m.group(2));
					// chapter1:verse1-chapter2:verse2
					refs = getVerseReferences(BookOfBible.getBookOfBible(book),
							chapter1, verse1);
				} else if ((m = ccPattern.matcher(theRest)).matches()) {
					chapter1 = Integer.parseInt(m.group(1));
					chapter2 = Integer.parseInt(m.group(2));
					// chapter1-chapter2
					refs = getChapterReferences(
							BookOfBible.getBookOfBible(book), chapter1,
							chapter2);
				} else if ((m = cvvPattern.matcher(theRest)).matches()) {
					chapter1 = Integer.parseInt(m.group(1));
					verse1 = Integer.parseInt(m.group(2));
					verse2 = Integer.parseInt(m.group(3));
					refs = getPassageReferences(
							BookOfBible.getBookOfBible(book), chapter1, verse1,
							verse2);
				} else if ((m = ccvPattern.matcher(theRest)).matches()) {
					chapter1 = Integer.parseInt(m.group(1));
					chapter2 = Integer.parseInt(m.group(2));
					verse1 = Integer.parseInt(m.group(3));
					refs = getPassageReferences(
							BookOfBible.getBookOfBible(book), chapter1, 1,
							chapter2, verse1);
				}
			} else {
				
			}
			return refs;
		} catch (Exception e) {
			return refs;
		}
	}

	// -----------------------------------------------------------------------------
	// The next set of methods are for use by the getReferencesForPassage method
	// above.
	// After it parses the input string it will call one of these.
	//
	// These methods should be somewhat easy to implement. They are kind of delegate
	// methods in that they call a method on the Bible class to do most of the work.
	// However, they need to do so for every version of the Bible stored in the
	// model.
	// and combine the results.
	//
	// Once you implement one of these, the rest of them should be fairly
	// straightforward.
	// Think before you code, get one to work, and then implement the rest based on
	// that one.
	// -----------------------------------------------------------------------------

	@Override
	public ArrayList<Reference> getVerseReferences(BookOfBible book, int chapter, int verse) {
		ArrayList<Reference> refs = new ArrayList<Reference>();
		Reference r = new Reference(book, chapter, verse);
		for (Bible bible : bibleList) {
			if (bible.isValid(r)) {
				refs.add(r);
				return refs;
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(Reference startVerse, Reference endVerse) {
		TreeSet<Reference> tree = new TreeSet<Reference>();
		for (Bible bible : bibleList) {
			if(!(bible.isValid(endVerse))){
				tree.addAll(bible.getReferencesExclusive(startVerse, endVerse));
			}		
			tree.addAll(bible.getReferencesInclusive(startVerse, endVerse));
		}
		ArrayList<Reference> refs = new ArrayList<Reference>(tree);
		return refs;
	}

	@Override
	public ArrayList<Reference> getBookReferences(BookOfBible book) {
		TreeSet<Reference> tree = new TreeSet<Reference>();
		if (book != null) {
			for (Bible bible : bibleList) {
				tree.addAll(bible.getReferencesForBook(book));
			}
		}
		ArrayList<Reference> refs = new ArrayList<Reference>(tree);
		return refs;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter) {
		TreeSet<Reference> tree = new TreeSet<Reference>();
		for (Bible bible : bibleList) {
				tree.addAll(bible.getReferencesForChapter(book, chapter));	
		}
		ArrayList<Reference> refs = new ArrayList<Reference>(tree);
		return refs;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter1, int chapter2) {
		TreeSet<Reference> tree = new TreeSet<Reference>();
		for (Bible bible : bibleList) {
				tree.addAll(bible.getReferencesForChapters(book, chapter1, chapter2));
		}
		ArrayList<Reference> refs = new ArrayList<Reference>(tree);
		return refs;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter, int verse1, int verse2) {
		TreeSet<Reference> tree = new TreeSet<Reference>();
		for (Bible bible : bibleList) {
			tree.addAll(bible.getReferencesForPassage(book, chapter, verse1, verse2));
		}
		ArrayList<Reference> refs = new ArrayList<Reference>(tree);
		return refs;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		TreeSet<Reference> tree = new TreeSet<Reference>();
		for (Bible bible : bibleList) {
				tree.addAll(bible.getReferencesForPassage(book, chapter1, verse1,
						chapter2, verse2));
		}
		ArrayList<Reference> refs = new ArrayList<Reference>(tree);
		return refs;
	}

	// ------------------------------------------------------------------
	// These are the better searching methods.
	//
	@Override
	public ArrayList<Reference> getReferencesContainingWord(String word) {
		ArrayList<Reference> refSet = new ArrayList<Reference>();
		for (Bible bible : bibleVersions.keySet()) {
			Concordance concordance = bibleVersions.get(bible);
			for(Reference reference : concordance.getReferencesContaining(word)){
			refSet.add(reference);
			}
		}
		//ArrayList<Referenece> ref = new ReferenceList(refSet);
		return refSet;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWords(String words) {
		ArrayList<Reference> refSet = new ArrayList<Reference>();
		ArrayList<String> word = new ArrayList<String>(Arrays.asList(words.split(" ")));
		for (Bible bible : bibleVersions.keySet()) {
			Concordance concordance = bibleVersions.get(bible);
			for(Reference reference : concordance.getReferencesContainingAll(word)){
			refSet.add(reference);
			}
		}
		//ReferenceList ref = new ReferenceList(refSet);
		return refSet;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWordsAndPhrases(String words) {
		return getReferencesContainingAllWords(words);
	}
}
