package bibleReader;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import bibleReader.model.BibleReaderModel;
import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.ResultType;

/**
 * The display panel for the Bible Reader.
 * 
 * @author Angel Iniguez & Ryan Yonker
 */
@SuppressWarnings("serial")
public class ResultView extends JPanel {

	private BibleReaderModel myModel;
	JScrollPane scrollPane;
	JEditorPane editorPane;
	JLabel statLabel;
	StringBuffer sb;
	private Box box;
	private JButton previous;
	private JButton next;
	private NavigableResults naWindow;

	/**
	 * Construct a new ResultView and set its model to myModel. It needs to
	 * model to look things up.
	 * 
	 * @param myModel0
	 *            The model this view will access to get information.
	 */
	public ResultView(BibleReaderModel myModel) {

		this.myModel = myModel;
		this.setLayout(new BorderLayout());

		box = new Box(BoxLayout.X_AXIS);
		previous = new JButton("Previous");
		previous.setName("PreviousButton");
		previous.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(naWindow.getType()==ResultType.SEARCH) {
					setSearchResults(naWindow.previousResults(), naWindow.getQueryPhrase());
					setSearchStats(naWindow.size(), naWindow.getQueryPhrase());
				} else if(naWindow.getType()==ResultType.PASSAGE){
					setPassageResults(naWindow.previousResults(),naWindow.getQueryPhrase());
					setPassageStats(naWindow.size(), naWindow.getQueryPhrase());
				}
				updateButtons();
			}
		});

		next = new JButton("Next");
		next.setName("NextButton");
		next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(naWindow.getType()==ResultType.SEARCH) {
					setSearchResults(naWindow.nextResults(), naWindow.getQueryPhrase());	
					setSearchStats(naWindow.size(), naWindow.getQueryPhrase());
				} else if(naWindow.getType()==ResultType.PASSAGE){
					setPassageResults(naWindow.nextResults(),naWindow.getQueryPhrase());
					setPassageStats(naWindow.size(), naWindow.getQueryPhrase());
				} 
				updateButtons();
			}
		});

		box.add(previous);
		box.add(next);
		statLabel = new JLabel();
		editorPane = new JEditorPane();
		editorPane.setContentType("text/html");
		editorPane.setEditable(false);
		scrollPane = new JScrollPane(editorPane);
		this.add(box, BorderLayout.NORTH);
		this.add(scrollPane, BorderLayout.CENTER);
		this.add(statLabel, BorderLayout.SOUTH);
		editorPane.setBackground(Color.getHSBColor(17, 52, 100));
		editorPane.setName("OutputEditorPane");
	}

	public void setResults(NavigableResults results) {
		naWindow = results;
		if(naWindow.getType()==ResultType.SEARCH) {
			setSearchResults(naWindow.currentResults(), naWindow.getQueryPhrase());		
			setSearchStats(naWindow.size(), naWindow.getQueryPhrase());
		} else {
			setPassageResults(naWindow.currentResults(),naWindow.getQueryPhrase());
			setPassageStats(naWindow.size(), naWindow.getQueryPhrase());
		}
		updateButtons();
	}

	private void updateButtons() {
		if (naWindow.hasPreviousResults()) {
			previous.setEnabled(true);
		} else {
			previous.setEnabled(false);
		}
		if (naWindow.hasNextResults()) {
			next.setEnabled(true);
		} else{
			next.setEnabled(false);
		}
	}
	/**
	 * Creates a table to be displayed with reference on far left and the verse
	 * text on the right
	 * 
	 * @param ReferenceList
	 *            VerseList from the BibleReaderApp of the verses containing the
	 *            searchString
	 * @param String
	 *            The string that was searched for
	 */
	public void setSearchResults(ArrayList<Reference> refs, String searchString) {
		sb = new StringBuffer();
		sb.append("<html><head></head><body style=\"background-color:#E9967A\"><table><tbody><tr><td>Verse</td>");
		for (String bible : myModel.getVersions()) {
			sb.append("<td><strong>" + bible + "</strong></td>");
		}
			for (Reference ref : refs) {
			sb.append("<tr><td>" + ref + "</td>");
			for (int n = 0; n < myModel.getNumberOfVersions(); n++) {
				sb.append("<td>"
						+ myModel.getText(myModel.getVersions()[n], ref)
								.replaceAll("(?i)" + searchString,  "<b>$0</b>")
						+ "</td>");
				//"(?!\\w)",
				//(?<!\\w)
			}
		}
		sb.append("</tr></tbody></table></body></html>");
		editorPane.setText(sb.toString()); // set text to string buffer
		editorPane.setCaretPosition(0);

	}

	public void setPassageResults(ArrayList<Reference> refs, String searchString) {
		sb = new StringBuffer();
		sb.append(
				"<html><body style=\"background-color:#E9967A\"><h2 style=\"text-align:center;font-weight:bold; \">");
		if(refs.isEmpty()){
			updateButtons();
		}
		else{
		if(!(refs.get(0).getBookOfBible().equals(refs.get(refs.size() - 1).getBookOfBible()))){
			sb.append(refs.get(0).toString() + "-"
							+ refs.get(refs.size() - 1).toString()
							+ "</h2><table><tbody><tr valign=\"top\">");
		}
		else if((!(refs.get(0).getChapter() == refs.get(refs.size() - 1).getChapter())) && 
				(refs.get(0).getBookOfBible().equals(refs.get(refs.size() - 1).getBookOfBible()))){
			sb.append(refs.get(0).toString() + "-" 
						+ refs.get(refs.size() - 1).getChapter() + ":"
						+ refs.get(refs.size() - 1).getVerse()
						+ "</h2><table><tbody><tr valign=\"top\">");
		}
		else if(((refs.get(0).getChapter() == refs.get(refs.size() - 1).getChapter())) && 
				(refs.get(0).getBookOfBible().equals(refs.get(refs.size() - 1).getBookOfBible()))){
			sb.append(refs.get(0).toString() + "-" 
					+ refs.get(refs.size() - 1).getVerse()
					+ "</h2><table><tbody><tr valign=\"top\">");
		}
		for(int n = 0; n < myModel.getNumberOfVersions(); n++) {
				sb.append("<td><strong>");
				sb.append(myModel.getVersions()[n] + "</strong><br>");
				for (Reference ref : refs) {
					if(!(myModel.getText(myModel.getVersions()[n], ref).isEmpty())){
						if(ref.getVerse() == 1){
							sb.append("<p><sup><strong>" + ref.getChapter() + "</strong></sup>" + myModel.getText(myModel.getVersions()[n], ref)
									.replaceAll("(?i)" + searchString, "<b>$0</b>"));
						}
						else{
							sb.append("<sup>" + ref.getVerse() + "</sup>" + myModel.getText(myModel.getVersions()[n], ref)
								.replaceAll("(?i)" + searchString, "<b>$0</b>"));
						}
					}
				}
				sb.append("</p></td>");
			}
		sb.append("</tr></tbody></table></body></html>");
		editorPane.setText(sb.toString()); // set text to string buffer
		editorPane.setCaretPosition(0);
		}
	}

	public void setSearchStats(int size, String searchString) {
			statLabel.setText("Displaying page " + naWindow.getPageNumber() + " of " + naWindow.getNumberPages() +". There are " + size + " verses that contain " + searchString);
	}

	public void setPassageStats(int size, String searchString) {
		if (size == 0) {
			statLabel.setText(searchString + " is an invalid passage. Please check your references.");
		} else {
			statLabel.setText("Displaying page " + naWindow.getPageNumber() + " of " + naWindow.getNumberPages() +". There are " + size + " verses for " + searchString);
		}

	}
}
