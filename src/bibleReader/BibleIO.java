package bibleReader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author cusack
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */

	public static VerseList readBible(File bibleFile) { 
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);
		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. The format is
	 * described below.
	 * 
	 * @param bibleFile
	 *            The file containing a Bible with .atv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if
	 *         there was an error reading the file.
	 */
	private static VerseList readATV(File bibleFile) {
		if (bibleFile.exists()) {
			try {
				VerseList verses = null;
				String[] character;
				String[] characters;
				String string = "";
				FileReader theReader = new FileReader(bibleFile);
				BufferedReader line = new BufferedReader(theReader);
				if ((string = line.readLine()) != null) {
					if (string.contains(":")) {
						character = string.split(": ");
						verses = new VerseList(character[0], character[1]);
					} else if (!(string.contains(": "))) {
						if (!(string.isEmpty())) {
							verses = new VerseList(string, "");
						}

						else {
							verses = new VerseList("unknown", "");
						}
					}
				}
				while ((string = line.readLine()) != null) {
					characters = string.split(("[@:]"), 4);
					if((BookOfBible.getBookOfBible(characters[0])) == null){
						line.close();
						return null;
					}
					Reference reference = new Reference(
							BookOfBible.getBookOfBible(characters[0]),
							Integer.parseInt(characters[1]),
							Integer.parseInt(characters[2]));
					verses.add(new Verse(reference, characters[3]));
				}
				line.close();
				return verses;
			} catch (Exception ex) {
				System.out.println("The file was not found");
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Read in the Bible that is stored in the XMV format.
	 * 
	 * @param bibleFile
	 *            The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if
	 *         there was an error reading the file.
	 */
	private static VerseList readXMV(File bibleFile) {
		VerseList verses = null;
		String[] characters;
		String[] character;
		String[] books;
		String[] chapterNumber;
		String[] verseNumber;
		BookOfBible book = null;
		Reference ref;
		Verse verseObject = null;
		int chapter = 0;
		int verse = 0;
		String[] verseTextCharacters;
		String verseText;
		String string = "";
		if (bibleFile.exists()) {
			try {
				FileReader theReader = new FileReader(bibleFile);
				BufferedReader line = new BufferedReader(theReader);
				if((string = line.readLine()) != null){
					if (!(string.startsWith("<Version"))) {
						verses = new VerseList("unknown", "");
						line.close();
						return verses;
					}
					else{
						character = string.split("<|\\s+|: ");
						characters = string.split("<|: ");
						verses = new VerseList(character[2], characters[2]);
					}
				}

				while ((string = line.readLine()) != null) {
					if (string.startsWith("<Book")) {
						books = string.split("<Book |>|, ");
						book = BookOfBible.getBookOfBible(books[1]);

					}
					if (string.startsWith("<Chapter")) {
						chapterNumber = string.split("<|>|\\s+");
						chapter = Integer.parseInt(chapterNumber[2]);

					}
					if (string.startsWith("<Title")) {

					}
					if (string.startsWith("<Verse")) {
						verseNumber = string.split("<|>|\\s+");
						verse = Integer.parseInt(verseNumber[2]);
						ref = new Reference(book, chapter, verse);
						verseTextCharacters = string.split(">");
						verseText = verseTextCharacters[1];

						verseObject = new Verse(ref, verseText.trim());
						ref = new Reference(book, chapter, verse);
						verses.add(verseObject);

					}
				}
				
				line.close();
				return verses;

			} catch (Exception ex) {
				ex.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file
	 *            The file that the Bible should be written to.
	 * @param bible
	 *            The Bible that will be written to the file.
	 */
	public static void writeBibleATV(File file, Bible bible) {
		try {
			FileWriter filewriter = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(filewriter);
			bw.append(bible.getVersion() + ": " + bible.getTitle());
			bw.newLine();
			for (Verse verse : bible.getAllVerses()) {

				bw.append(verse.getReference().getBook());
				bw.append("@");
				bw.append(Integer.toString(verse.getReference().getChapter()));
				bw.append(":");
				bw.append(Integer.toString(verse.getReference().getVerse()));
				bw.append("@");
				bw.append(verse.getText());
				bw.newLine();
			}
			bw.close();
		} catch (IOException ex) {
			System.out.println("Could not read file");
		}
	}

	/**
	 * Write out the given verses in the ATV format, using the description as
	 * the first line of the file.
	 * 
	 * @param file
	 *            The file that the Bible should be written to.
	 * @param description
	 *            The contents that will be placed on the first line of the
	 *            file, formatted appropriately.
	 * @param verses
	 *            The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {
		try {
			FileWriter filewriter = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(filewriter);

			bw.append(description);
			bw.newLine();

			for (Verse verse : verses) {

				bw.append(verse.getReference().getBook());
				bw.append("@");
				bw.append(Integer.toString(verse.getReference().getChapter()));
				bw.append(":");
				bw.append(Integer.toString(verse.getReference().getVerse()));
				bw.append("@");
				bw.append(verse.getText());
				bw.newLine();
			}
			bw.close();
		} catch (IOException ex) {
			System.out.println("Could not read file");
		}
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is
	 * an HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {
		try {
			FileWriter filewriter = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(filewriter);
			bw.append(text);
			bw.close();
		} catch (Exception ex) {
			System.out.println("Could not read file");
		}
	}
}
